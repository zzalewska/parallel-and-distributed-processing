#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<pthread.h>

#include"czytelnia.h"


/*** Implementacja procedur interfejsu ***/

int my_read_lock_lock(czytelnia_t* czytelnia){
	pthread_mutex_lock(&czytelnia->muteks);
	if(czytelnia->liczba_pisz>0 || czytelnia->pisz_e!=0){
		czytelnia->czyt_e++;
		pthread_cond_wait(&czytelnia->czyt, &czytelnia->muteks);
		czytelnia->czyt_e--;}
	czytelnia->liczba_czyt++;
	pthread_cond_signal(&czytelnia->czyt);
	pthread_mutex_unlock(&czytelnia->muteks);
	if(czytelnia->liczba_pisz>0){
	printf("Liczba pisarzy %d wieksza od 0", czytelnia->liczba_pisz);
	exit(0);}
  
}


int my_read_lock_unlock(czytelnia_t* czytelnia){
   pthread_mutex_lock(&czytelnia->muteks);
	czytelnia->liczba_czyt--;
	if(czytelnia->liczba_czyt==0){
		pthread_cond_signal(&czytelnia->pisz);
	}
	pthread_mutex_unlock(&czytelnia->muteks);
}


int my_write_lock_lock(czytelnia_t* czytelnia){
	pthread_mutex_lock(&czytelnia->muteks);
	if(czytelnia->liczba_pisz+czytelnia->liczba_czyt>0){
	czytelnia->pisz_e++;		
	pthread_cond_wait(&czytelnia->pisz, &czytelnia->muteks);
	czytelnia->pisz_e--;}
	czytelnia->liczba_pisz++;
	pthread_mutex_unlock(&czytelnia->muteks);
	if(czytelnia->liczba_pisz>1 || czytelnia->liczba_czyt>0){
	printf("Liczba pisarzy i czytelnikow wieksza od 0");
	exit(0);}
  
}


int my_write_lock_unlock(czytelnia_t* czytelnia){
    pthread_mutex_lock(&czytelnia->muteks);
	czytelnia->liczba_pisz--;
	if(czytelnia->czyt_e!=0){
		pthread_cond_signal(&czytelnia->czyt);}
else{
	pthread_cond_signal(&czytelnia->pisz);}
	pthread_mutex_unlock(&czytelnia->muteks);

}

void inicjuj(czytelnia_t* czytelnia){
	czytelnia->liczba_pisz=0;
	czytelnia->liczba_czyt=0;
	pthread_cond_init(&czytelnia->pisz, NULL);
	pthread_cond_init(&czytelnia->czyt, NULL);
	pthread_mutex_init(&czytelnia->muteks, NULL);
	czytelnia->czyt_e=0;
	czytelnia->pisz_e=0;

}

void czytam(czytelnia_t* czytelnia_p){
    usleep(rand()%1000000);
}

void pisze(czytelnia_t* czytelnia_p){
    usleep(rand()%1000000);
}


