#include<stdlib.h>
#include<stdio.h>
#include <time.h>
#include<pthread.h>
#include <stdbool.h>
#include <unistd.h>

pthread_mutex_t kufel_mutex;
pthread_mutex_t kran_mutex;

bool* tab_kufle;
bool* tab_krany;
int l_kufli;
int l_kranow;
int l_wolnych_kufli;
int l_wolnych_kranow;

void * watek_klient (void * arg);

main(){
	
	srand(time(NULL));
	pthread_mutex_init(&kufel_mutex,NULL);
	pthread_mutex_init(&kran_mutex,NULL);
  pthread_t *tab_klient;
  int *tab_klient_id;

  int l_kl, l_kf, l_kr, i;

  printf("\nLiczba klientow: "); scanf("%d", &l_kl);

  printf("\nLiczba kufli: "); scanf("%d", &l_kf);
  tab_kufle=(bool*) malloc(l_kf*sizeof(bool));
  for (int i=0;i<l_kf;i++){
	  tab_kufle[i]=true;
  }
  l_kufli=l_kf;
  l_wolnych_kufli=l_kufli;
  printf("\nLiczba kranow: ");
  scanf("%d", &l_kr);
  l_wolnych_kranow=l_kr;
  l_kranow=l_kr;
  tab_krany=(bool*) malloc(l_kr*sizeof(bool));
    for (int i=0;i<l_kr;i++){
	  tab_krany[i]=true;
  }
  tab_klient = (pthread_t *) malloc(l_kl*sizeof(pthread_t));
  tab_klient_id = (int *) malloc(l_kl*sizeof(int));
  for(i=0;i<l_kl;i++) tab_klient_id[i]=i;


  printf("\nOtwieramy pub (simple)!\n");
  printf("\nLiczba wolnych kufli %d\n", l_kf); 

  for(i=0;i<l_kl;i++){
    pthread_create(&tab_klient[i], NULL, watek_klient, &tab_klient_id[i]); 
  }
  for(i=0;i<l_kl;i++){
    pthread_join( tab_klient[i], NULL);
  }
  printf("\nZamykamy pub!\n");


}


void * watek_klient (void * arg_wsk){

  int moj_id = * ((int *)arg_wsk);

  int i, j, kufel, kran, result;
  int ile_musze_wypic = 2;
  printf("\nKlient %d, wchodzę do pubu\n", moj_id); 
    
  for(i=0; i<ile_musze_wypic; i++){
	
	do{
		result=pthread_mutex_trylock(&kufel_mutex);
		if (result==0){
//			printf("l_kufli %d \n",l_wolnych_kufli);
			if (l_wolnych_kufli>0){
				for (int i=0;i<l_kufli;i++){
					if (tab_kufle[i]){
						tab_kufle[i]=false;
						l_wolnych_kufli--;
						kufel=i;
						printf("\nKlient %d, wybieram kufel %d\n", moj_id,kufel);
						printf("\nLiczba wolnych kufli: %d\n", l_wolnych_kufli);
						break;
					}
				}
				pthread_mutex_unlock(&kufel_mutex);
				break;
			}
			pthread_mutex_unlock(&kufel_mutex);
		}
		j=(rand()%21+10)*10000;
		printf("Klient %d, czekam %d us na kufel\n",moj_id,j);
		usleep(j);
	} while (true);

	do {
		result=pthread_mutex_trylock(&kran_mutex);
		if (result==0){
			if(l_wolnych_kranow>0){
				for (int i=0;i<l_kranow;i++){
					if (tab_krany[i]){
						tab_krany[i]=false;
						l_wolnych_kranow--;
						kran=i;
						printf("\nKlient %d, nalewam z kranu %d\n", moj_id, kran); 
						printf("\nLiczba wolnych kranow: %d\n", l_wolnych_kranow);
						break;
					}
				}
				pthread_mutex_unlock(&kran_mutex);
				break;
			}
			pthread_mutex_unlock(&kran_mutex);
		}
		j=(rand()%21+10)*10000;
		printf("Klient %d, czekam %d us na kran\n",moj_id,j);
		usleep(j);
	} while (true);
	
//	Nalewanie z kranu
    usleep(300);
    
    do{
		result=pthread_mutex_trylock(&kran_mutex);
		if (result==0){
			tab_krany[kran]=true;
			l_wolnych_kranow++;
			printf("\nKlient %d, koniec nalewania z kranu %d\n", moj_id,kran);
			printf("\nLiczba wolnych kranow: %d\n", l_wolnych_kranow);
			pthread_mutex_unlock(&kran_mutex);
			break;
		}
		j=(rand()%21+10)*10000;
		printf("Klient %d, czekam %d us, by powiedzieć, że skończyłem nalewać z kranu: %d\n",moj_id,j,kran);
		usleep(j);
	} while (true); 
    
//     
	if(kran==0)  printf("\nKlient %d, pije piwo Guinness z kranu %d\n", moj_id, kran); 
    else if(kran==1)  printf("\nKlient %d, pije piwo Żywiec z kranu %d\n", moj_id, kran); 
    else if(kran==2)  printf("\nKlient %d, pije piwo Heineken z kranu %d\n", moj_id, kran); 
    else if(kran==3)  printf("\nKlient %d, pije piwo Okocim z kranu %d\n", moj_id, kran); 
    else if(kran==4)  printf("\nKlient %d, pije piwo Karlsberg z kranu %d\n", moj_id, kran); 
	else printf("\nKlient %d, pije piwo z kranu %d\n", moj_id, kran);
    nanosleep((struct timespec[]){{0, 500000000L}}, NULL);
    

	do{
		result=pthread_mutex_trylock(&kufel_mutex);
		if (result==0){
			tab_kufle[kufel]=true;
			l_wolnych_kufli++;
			printf("\nKlient %d, odkladam kufel %d\n", moj_id,kufel);
			printf("\nLiczba wolnych kufli: %d\n", l_wolnych_kufli);
			pthread_mutex_unlock(&kufel_mutex);
			break;
		}
		j=(rand()%21+10)*10000;
		printf("Klient %d, czekam %d us, na odłożenie kufla: %d\n",moj_id,j,kufel);
		usleep(j);

	} while (true); 
	}
  printf("\nKlient %d, wychodzę z pubu\n", moj_id); 
    
  return(NULL);
} 


