#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>

#define WYMIAR 13

main ()
{
  double a[WYMIAR][WYMIAR];
  int n,i,j;

  for(i=0;i<WYMIAR;i++) for(j=0;j<WYMIAR;j++) a[i][j]=1.02*i+1.01*j;

  n=WYMIAR;

  double suma=0.0;
  for(i=0;i<WYMIAR;i++) {
    for(j=0;j<WYMIAR;j++) {
      suma += a[i][j];
    }
  }
 
  printf("Suma wyrazów tablicy: %lf\n", suma);


  omp_set_nested(1);
//1 - wierszowo reduction
  double suma_parallel1=0.0;
  #pragma omp parallel for default(none) private(j) shared(a) reduction(+:suma_parallel1) ordered
  for(i=0;i<WYMIAR;i++) {
    int id_w = omp_get_thread_num();
    for(j=0;j<WYMIAR;j++) {
      suma_parallel1 += a[i][j];
#pragma omp ordered
      printf("(%2d,%2d)-W(%1d,%1d) ",i,j,id_w,omp_get_thread_num());
    }
    printf("\n");
  }

  printf("Suma wyrazów tablicy równolegle wierszowo klauzula reduction: %lf\n", suma_parallel1);


//2 - wierszowo tablica
 
  double suma_parallel2=0.0;
  double tab[WYMIAR];
 
  for (i=0; i<WYMIAR; i++)
{
 int id_w = omp_get_thread_num();
 int x=0;
 for(x=0; x<WYMIAR; x++) tab[x]=0;
 #pragma omp parallel for default(none) firstprivate(i, id_w) shared(a, tab) reduction(+:suma_parallel2) ordered
 for(j=0; j<WYMIAR; j++)
 {
 suma_parallel2+=a[i][j];
 tab[omp_get_thread_num()] = a[i][j];
 #pragma omp ordered
 printf("{%2d,%2d)-W(%1d, %1d)", i, j, id_w, omp_get_thread_num());
}
printf("\n");
}

printf("Suma wyrazów tablicy (dla tablicy): %lf\n", suma_parallel2);



//3 kolumnowo reduction
 
  double suma_parallel3= 0.0;
  for (i=0; i<WYMIAR; i++)
{
 int id_w = omp_get_thread_num();
 #pragma omp parallel for default(none) firstprivate(i) shared(a) reduction(+:suma_parallel3) firstprivate(id_w) ordered
 for(j=0; j<WYMIAR; j++)
 {
 suma_parallel3 += a[j][i];
 #pragma omp ordered
 printf("{%2d,%2d)-W(%1d, %1d)", i, j, id_w, omp_get_thread_num());
}
//...
printf("\n");
}
printf("Suma wyrazów tablicy kolumnowo dla reduction: %lf\n", suma_parallel3);

//4
  double suma_parallel4=0.0;
  double sumap=0.0;
  #pragma omp parallel for default(none) private(j, sumap) shared(a, suma_parallel4) ordered
  for (i=0; i<WYMIAR; i++)
{
 int id_w = omp_get_thread_num();
 sumap=0.0;
 for(j=0; j<WYMIAR; j++)
 {
 sumap+= a[j][i];
 #pragma omp ordered
 printf("{%2d,%2d)-W(%1d, %1d)", i, j, id_w, omp_get_thread_num());
}
#pragma omp critical
{
suma_parallel4+=sumap;
}
printf("\n");
}

printf("Suma wyrazów tablicy kolumnowo zmienna prywatna: %lf\n", suma_parallel4);



//dla static porcja=3

//1 - wierszowo reduction porcja 3
  double suma_parallel12=0.0;
  #pragma omp parallel for default(none) private(j) shared(a) reduction(+:suma_parallel12) schedule(static,3) ordered
  for(i=0;i<WYMIAR;i++) {
    int id_w = omp_get_thread_num();
    for(j=0;j<WYMIAR;j++) {
      suma_parallel12 += a[i][j];
#pragma omp ordered
      printf("(%2d,%2d)-W(%1d,%1d) ",i,j,id_w,omp_get_thread_num());
    }
    printf("\n");
  }

  printf("Suma wyrazów tablicy równolegle wierszowo klauzula reduction static=3: %lf\n", suma_parallel12);


//2 - wierszowo tablica
 
  double suma_parallel22=0.0;
  double tab2[WYMIAR];
 
  for (i=0; i<WYMIAR; i++)
{
 int id_w = omp_get_thread_num();
 int x=0;
 for(x=0; x<WYMIAR; x++) tab[x]=0;
 #pragma omp parallel for default(none) firstprivate(i, id_w) shared(a, tab2) reduction(+:suma_parallel22) schedule(static,3) ordered
 for(j=0; j<WYMIAR; j++)
 {
 suma_parallel22+=a[i][j];
 tab2[omp_get_thread_num()] = a[i][j];
 #pragma omp ordered
 printf("{%2d,%2d)-W(%1d, %1d)", i, j, id_w, omp_get_thread_num());
}
printf("\n");
}

printf("Suma wyrazów tablicy (dla tablicy) static=3: %lf\n", suma_parallel22);



//3 kolumnowo reduction
 
  double suma_parallel32= 0.0;
  for (i=0; i<WYMIAR; i++)
{
 int id_w = omp_get_thread_num();
 #pragma omp parallel for default(none) firstprivate(i) shared(a) reduction(+:suma_parallel32) firstprivate(id_w) schedule(static,3) ordered
 for(j=0; j<WYMIAR; j++)
 {
 suma_parallel32 += a[j][i];
 #pragma omp ordered
 printf("{%2d,%2d)-W(%1d, %1d)", i, j, id_w, omp_get_thread_num());
}
//...
printf("\n");
}
printf("Suma wyrazów tablicy kolumnowo dla reduction static=3: %lf\n", suma_parallel32);

//4
  double suma_parallel42=0.0;
  double sumap2=0.0;
  #pragma omp parallel for default(none) private(j, sumap2) shared(a, suma_parallel42) schedule(static, 3) ordered
  for (i=0; i<WYMIAR; i++)
{
 int id_w = omp_get_thread_num();
 sumap2=0.0;
 for(j=0; j<WYMIAR; j++)
 {
 sumap2+= a[j][i];
 #pragma omp ordered
 printf("{%2d,%2d)-W(%1d, %1d)", i, j, id_w, omp_get_thread_num());
}
#pragma omp critical
{
suma_parallel42+=sumap2;
}
printf("\n");
}

printf("Suma wyrazów tablicy kolumnowo zmienna prywatna static=3: %lf\n", suma_parallel42);

//dla static domylsnego

//1 - wierszowo reduction porcja 3
  double suma_parallel13=0.0;
  #pragma omp parallel for default(none) private(j) shared(a) reduction(+:suma_parallel13) schedule(static) ordered
  for(i=0;i<WYMIAR;i++) {
    int id_w = omp_get_thread_num();
    for(j=0;j<WYMIAR;j++) {
      suma_parallel13 += a[i][j];
#pragma omp ordered
      printf("(%2d,%2d)-W(%1d,%1d) ",i,j,id_w,omp_get_thread_num());
    }
    printf("\n");
  }

  printf("Suma wyrazów tablicy równolegle wierszowo klauzula reduction static: %lf\n", suma_parallel13);


//2 - wierszowo tablica
 
  double suma_parallel23=0.0;
  double tab3[WYMIAR];
 
  for (i=0; i<WYMIAR; i++)
{
 int id_w = omp_get_thread_num();
 int x=0;
 for(x=0; x<WYMIAR; x++) tab[x]=0;
 #pragma omp parallel for default(none) firstprivate(i, id_w) shared(a, tab3) reduction(+:suma_parallel22) schedule(static) ordered
 for(j=0; j<WYMIAR; j++)
 {
 suma_parallel22+=a[i][j];
 tab3[omp_get_thread_num()] = a[i][j];
 #pragma omp ordered
 printf("{%2d,%2d)-W(%1d, %1d)", i, j, id_w, omp_get_thread_num());
}
printf("\n");
}

printf("Suma wyrazów tablicy (dla tablicy) static=3: %lf\n", suma_parallel22);



//3 kolumnowo reduction
 
  double suma_parallel33= 0.0;
  for (i=0; i<WYMIAR; i++)
{
 int id_w = omp_get_thread_num();
 #pragma omp parallel for default(none) firstprivate(i) shared(a) reduction(+:suma_parallel33) firstprivate(id_w) schedule(static) ordered
 for(j=0; j<WYMIAR; j++)
 {
 suma_parallel33 += a[j][i];
 #pragma omp ordered
 printf("{%2d,%2d)-W(%1d, %1d)", i, j, id_w, omp_get_thread_num());
}
//...
printf("\n");
}
printf("Suma wyrazów tablicy kolumnowo dla reduction static: %lf\n", suma_parallel33);

//4
  double suma_parallel43=0.0;
  double sumap3=0.0;
  #pragma omp parallel for default(none) private(j, sumap3) shared(a, suma_parallel43) schedule(static) ordered
  for (i=0; i<WYMIAR; i++)
{
 int id_w = omp_get_thread_num();
 sumap3=0.0;
 for(j=0; j<WYMIAR; j++)
 {
 sumap3+= a[j][i];
 #pragma omp ordered
 printf("{%2d,%2d)-W(%1d, %1d)", i, j, id_w, omp_get_thread_num());
}
#pragma omp critical
{
suma_parallel43+=sumap3;
}
printf("\n");
}

printf("Suma wyrazów tablicy kolumnowo zmienna prywatna static: %lf\n", suma_parallel43);

 
}
