#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>


typedef struct {
	int ident;
	char* napis;
	char typ;
} Struktura;


void* pthreadFunc(void* arg){
	Struktura* str=(Struktura*)arg;
	int ident=str->ident;
	printf("Przed modyfikacja:\npid: %lu\nid: %d\nnapis: %s\ntyp: %c\n\n",pthread_self(),ident,str->napis,str->typ);
	
	str->typ+=10;
	
	ident+=11;
	
	printf("Po modyfikacji:\npid: %lu\nid: %d\nnapis: %s\ntyp: %c\n\n",pthread_self(),ident,str->napis,str->typ);
	
}


int main(void){
	const int size=1000;
	
	pthread_t tab[size];
	void* result;
	
	Struktura str={51,"Moj napis ;)",'A'};
	
	for(int i=0;i<size;i++){
		pthread_create(&tab[i],NULL,pthreadFunc,(void*)&str);
	}
	
	for(int i=0;i<size;i++){
		pthread_join(tab[i],&result);
	}
	
	printf("Main:\npid: %lu\nid: %d\nnapis: %s\ntyp: %c\n\n",pthread_self(),str.ident,str.napis,str.typ);
}
